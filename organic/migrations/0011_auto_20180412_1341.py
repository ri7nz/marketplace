# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0010_auto_20180412_1337'),
    ]

    operations = [
        migrations.AddField(
            model_name='bank',
            name='Nbank',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bank',
            name='Name_receiver',
            field=models.ForeignKey(blank=True, to='organic.ListDelivery', null=True),
        ),
        migrations.AlterField(
            model_name='listdelivery',
            name='receiver',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
