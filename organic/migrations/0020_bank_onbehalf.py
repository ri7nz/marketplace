# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0019_auto_20180413_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='bank',
            name='onbehalf',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
