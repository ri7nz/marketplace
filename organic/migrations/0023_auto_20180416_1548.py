# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0022_history'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='listdelivery',
            name='cart',
        ),
        migrations.RemoveField(
            model_name='listdelivery',
            name='nbank',
        ),
        migrations.DeleteModel(
            name='Bank',
        ),
        migrations.DeleteModel(
            name='ListDelivery',
        ),
    ]
